# 15. 珍藏条变化 / きららジャンプゲージ量変化

![](../merge/Buff-ChainCoef.png) ![](../merge/Debuff-ChainCoef.png)

**`KiraraJumpGaugeChange`。**对玩家的珍藏条造成变化。

变化的数值直接作用在珍藏条上。当玩家角色使用该技能时，先执行该技能内容，再对技能本身增加珍藏条进行计算。

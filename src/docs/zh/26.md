# 26. 暴击伤害变化 / クリティカルダメージ量変化

![](../merge/Buff-CriticalDamage.png) ![](../merge/Debuff-CriticalDamage.png)

**`CriticalDamageChange`。**对暴击时的伤害造成影响。

## 计算方法

暴击伤害变化作用在伤害中的暴击系数上。暴击系数计算方法如下：

$$
暴击系数 = [1.5 \times (1 + 暴击伤害变化)] ^{3.0} _{1.0}
$$

当发生暴击时，伤害的结果与暴击系数相乘。

## 持续时间

按照目标自身的行动回合数量计算。多个暴击伤害变化之间独立存在，分别计算时间。

## 叠加方式

多个暴击伤害变化之间独立存在，作用效果数值相加。

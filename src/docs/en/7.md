# 7. 异常抵抗率 / 状態異常付与確率補正

![](../merge/Buff-AbnormalProbabilityDown.png) ![](../merge/Debuff-AbnormalProbabilityUp.png)

<WIP />

**`AbnormalAdditionalProbability`。**使得自身受各种异常的概率发生变化。

## 计算方式

$$
自身陷入异常概率 = 释放者技能概率 - 自身异常抵抗率
$$

异常抵抗率为正数时表示异常概率降低。

## 持续时间

按照目标自身的行动回合数量计算。多个状态变化之间独立存在，分别计算时间。

## 叠加方式

多个异常抵抗率变化之间独立存在，作用效果数值相加。

## 数值与技能量描述

异常抵抗率一般不会随着技能等级提升而成长。
